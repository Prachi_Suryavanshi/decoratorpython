from time import perf_counter


def time_it(func):
    def wrapper(*args, **kwargs):
        start = perf_counter()  # Start the stopwatch / counter
        result = func(*args, **kwargs)
        end = perf_counter()    # Stop the stopwatch / counter
        print(f"{func.__name__} took {end - start} seconds")
        return result

    return wrapper

# https://www.geeksforgeeks.org/time-perf_counter-function-in-python/
# https://realpython.com/primer-on-python-decorators/
